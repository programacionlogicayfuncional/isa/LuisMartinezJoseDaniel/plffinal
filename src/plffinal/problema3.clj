(ns plffinal.problema3
  (:require [plffinal.core :as p1]))

(def alfabeto{\< \>, \> \<,\v \^, \^ \v})

(defn regreso-al-punto-de-origen
  ([xs]
   (let [a (fn [vs] (p1/regresa-al-punto-de-origen? vs))]
     (if (a xs) (list)
         (reverse (map alfabeto xs))))))