(ns plffinal.core)

(defn calcular
  [xs]
  (frequencies xs))

(defn regresa-al-punto-de-origen?
  [xs]
  (let [a (calcular xs)]
    (or (empty? xs)
        (and (= (a \>) (a \<))
             (= (a \^) (a \v))))))
