(ns plffinal.problema2
  (:require [plffinal.core :as p1]))


(defn regresan-al-punto-de-origen?
  ([& args]
   (let [a (fn [xs]  (p1/regresa-al-punto-de-origen? xs))]
     (or (every? empty? args)
         (every? true? (map a args))))))
