(ns plffinal.problema4)

(def alfabeto {\> 1, \< -1, \^ 1, \v -1 })

(defn calcular-4
  [xs]
  (let [a (fn [vs] (apply + (map alfabeto vs)))]
    (a xs)))
(defn mismo-punto-final?
  ([xs ws]
   (or (and (empty? xs)
            (empty? ws))
       (= (calcular-4 xs) (calcular-4 ws)))))