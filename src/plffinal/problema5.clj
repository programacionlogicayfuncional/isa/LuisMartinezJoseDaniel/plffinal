(ns plffinal.problema5
  (:require [clojure.set :as set]))

(def alfabeto {\> 3, \< -3, \^ 1, \v -1})

(defn calcular-5
  ([xs]
   (let [a (fn [ws] (reductions + (map alfabeto ws)))]
     (a xs)
     )))
(defn coincidencias
  ([xs vs]
   (let [a (fn [] (set/intersection  (set (calcular-5 xs)) (set (calcular-5 vs))))
         b (if (or (empty? xs)
                   (empty? vs)) 1
               (inc (count (a))))]

     b)))

(coincidencias [\^ \> \> \> \^] ">^^<")