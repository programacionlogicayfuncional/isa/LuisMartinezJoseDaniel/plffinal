(ns plffinal.core-test
  (:require [clojure.test :refer :all]
            [plffinal.core :as p1]
            [plffinal.problema2 :as p2]
            [plffinal.problema3 :as p3]
            [plffinal.problema4 :as p4]
            [plffinal.problema5 :as p5]
            ))

(deftest regresa-al-punto-de-origen?-test
  (testing "Pruebas unitarias del problema 1"
    (is (true? (p1/regresa-al-punto-de-origen? "")))
    (is (true? (p1/regresa-al-punto-de-origen? [])))
    (is (true? (p1/regresa-al-punto-de-origen? (list))))
    (is (true? (p1/regresa-al-punto-de-origen? (list \> \<))))
    (is (true? (p1/regresa-al-punto-de-origen? "v^")))
    (is (true? (p1/regresa-al-punto-de-origen? [\v \^])))
    (is (true? (p1/regresa-al-punto-de-origen? "^>v<")))
    (is (true? (p1/regresa-al-punto-de-origen? (list \^ \> \v \<))))
    (is (true? (p1/regresa-al-punto-de-origen? "<<vv>>^^")))
    (is (true? (p1/regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])))

    (is (false? (p1/regresa-al-punto-de-origen? ">")))
    (is (false? (p1/regresa-al-punto-de-origen? (list \>))))
    (is (false? (p1/regresa-al-punto-de-origen? "<^")))
    (is (false? (p1/regresa-al-punto-de-origen? [\< \^])))
    (is (false? (p1/regresa-al-punto-de-origen? ">>><<")))
    (is (false? (p1/regresa-al-punto-de-origen? (list \> \> \> \< \<))))
    (is (false? (p1/regresa-al-punto-de-origen? [\v \v \^ \^ \^])))
    ))

(deftest regresan-al-punto-de-origen?-test
  (testing "Pruebas unitarias del problema 2"
    (is (true? (p2/regresan-al-punto-de-origen?)))
    (is (true? (p2/regresan-al-punto-de-origen? [])))
    (is (true? (p2/regresan-al-punto-de-origen? "")))
    (is (true? (p2/regresan-al-punto-de-origen? [] "" (list))))
    (is (true? (p2/regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")))
    (is (true? (p2/regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))))

    (is (false? (p2/regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])))
    (is (false? (p2/regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")))
    (is (false? (p2/regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])))))

(deftest regreso-al-punto-de-origen-test
  (testing "Pruebas de calculo del problema 3"
    (is (= () (p3/regreso-al-punto-de-origen "")))
    (is (= () (p3/regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))))
    (is (= (seq [\< \< \<]) (p3/regreso-al-punto-de-origen ">>>")))
    (is (= (seq [\< \< \^ \^ \^ \>]) (p3/regreso-al-punto-de-origen [\< \v \v \v \> \>])))
   ))

(deftest mismo-punto-final?-test
  (testing "Pruebas de calculo del problema 4"
    (is (true? (p4/mismo-punto-final? "" [])))
    (is (true? (p4/mismo-punto-final? "^^^" "<^^^>")))
    (is (true? (p4/mismo-punto-final? [\< \< \< \>] (list \< \<))))
    (is (true? (p4/mismo-punto-final? (list \< \v \>) (list \> \v \<))))

    (is (false? (p4/mismo-punto-final? "" "<")))
    (is (false? (p4/mismo-punto-final? [\> \>] "<>")))
    (is (false? (p4/mismo-punto-final? [\> \> \>] [\> \> \> \>])))
    (is (false? (p4/mismo-punto-final? (list) (list \^))))
    ))
(deftest coincidencias-test
  (testing "Pruebas de calculo del problema 5"
    (is (= 1 (p5/coincidencias "" [])))
    (is (= 1 (p5/coincidencias (list \< \<) [\> \>])))
    (is (= 2 (p5/coincidencias [\^ \> \> \> \^] ">^^<")))
    (is (= 4 (p5/coincidencias "<<vv>>^>>" "vv<^")))
    (is (= 6 (p5/coincidencias ">>>>>" [\> \> \> \> \>])))
    (is (= 6 (p5/coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))))))