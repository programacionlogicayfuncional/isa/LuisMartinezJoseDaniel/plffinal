(ns plffinal.problema3
   (:require [plffinal.core :as p1]))

(def alfabeto3 {\^ \v, \> \<, \v \^, \< \>})

;;Problema 3
;;--------------------------------------------------
(defn regreso-al-punto-de-origen
  ([xs]
   (let [a (fn [xs] (p1/regresa-al-punto-de-origen? xs))]
     (if (a xs) (list)
         (reverse (map alfabeto3 xs))))))
