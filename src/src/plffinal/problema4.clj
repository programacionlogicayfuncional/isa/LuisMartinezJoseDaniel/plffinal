(ns plffinal.problema4)
;;Problema4
;;--------------------------------------------------
(def alfabeto4 {\< -1, \v -1, \> 1, \^ 1})
(defn calcular-4
  [xs]
  (let [a (fn [ws] (apply + (map alfabeto4 ws)))]
    (a xs)))
(defn mismo-punto-final?
  ([xs ws]
   (or (and (empty? xs)
            (empty? ws))
       (= (calcular-4 xs) (calcular-4 ws)))))