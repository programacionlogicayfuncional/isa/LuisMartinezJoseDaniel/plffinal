(ns plffinal.problema5
  (:require [clojure.set :as set]))

(def alfabeto5 {\< -3, \v -1, \> 3, \^ 1})
(defn calcular-5
  [xs]
  (let [a (fn [ws] (reductions + (map alfabeto5 ws)))]
    (a xs)))

(defn coincidencias
  [vs xs]
  (let [a (fn []  (set/intersection (set (calcular-5 vs)) (set (calcular-5 xs))))
        b (fn [] (if (or (empty? vs)
                         (empty? xs)) 1
                   (inc (count (a)))))]
    (b)))